package frog

import (
  "net"
  "fmt"
  "log"
  "bufio"
  "errors"
  "strings"
  "strconv"
)
type Sentence map[string]Token

type FrogClient struct {
   Connection net.Conn
}
func NewFrogClient( address string ) ( *FrogClient, error ) {
    if conn, err := net.Dial( "tcp", address ); err != nil {
      return nil, err
    } else {
      return &FrogClient{ Connection: conn }, nil
    }
}

type Token struct {
    Position        int
    Token           string
    Lemma           string
    POSTag          string
    POSConfidence   float64
    NamedEntityType string
    POSEncoded      Tag
}

// we use 64-bits for encoding grammar info
type Tag uint64
// we use the top-8 or 9 bits for the type, the rest for subtypes
const TypeShift int = 53
const TypePart Tag = 1 << TypeShift
const TypePartMask Tag = 0b11111111111 << TypeShift

// for every type and subtype, we have English words, but also the
// most common Dutch words.
const (
  Noun                 Tag =   1 * TypePart // N
  ZelfstandigNaamwoord Tag =   1 * TypePart // N
    Common        Tag = 1 << 0 // soort, ntype
    Soort         Tag = 1 << 0 // soort, ntype
    Property      Tag = 1 << 1 // eigen, ntype
    Eigenschap    Tag = 1 << 1 // eigen, ntype
    NonNeuter     Tag = 1 << 2 // zijd
    Zijdig        Tag = 1 << 2 // zijd
    Genus         Tag = 1 << 3 // underspec genus
    Base          Tag = 1 << 4 // basis, graad
    Basis         Tag = 1 << 4 // basis, graad
    Diminutive    Tag = 1 << 5 // dim, graad
    Verkleinwoord Tag = 1 << 5 // dim, graad
    Neuter        Tag = 1 << 6 // onz
    Onzijdig      Tag = 1 << 6 // onz
    Singular      Tag = 1 << 7 // ev
    Enkelvoud     Tag = 1 << 7 // ev
    Plural        Tag = 1 << 8 // mv
    Meervoud      Tag = 1 << 8 // mv
    Standard      Tag = 1 << 9 // stan, naamval
    Standaard     Tag = 1 << 9 // stan, naamval
    EersteNaamval Tag = 1 << 9 // stan, naamval
    Genitive      Tag = 1 << 10 // gen, naamval
    Genitief      Tag = 1 << 10 // gen, naamval
    TweedeNaamval Tag = 1 << 10 // gen, naamval
    Dative        Tag = 1 << 28 // dat, naamval
    Datief        Tag = 1 << 28 // dat, naamval
    DerdeNaamval  Tag = 1 << 28 // dat, naamval

  Adjective            Tag =   2 * TypePart // Adj
  BijvoeglijkNaamwoord Tag =   2 * TypePart // Adj
    Superlative   Tag = 1 << 1 // sup, graad
    OvertreffendeTrap Tag= 1 << 1 // sup, graad
    NoDeclension  Tag = 1 << 3 // zonder, buiging
    Sterk         Tag = 1 << 3 // zonder, buiging
    //Base          Tag = 1 << 4 // basis, graad
    //Diminutive    Tag = 1 << 5 // dim, graad
    //Singular      Tag = 1 << 7 // zonder-n
    //Plural        Tag = 1 << 8 // mv-n
    //Standard      Tag = 1 << 9 // stan, naamval
    Special       Tag = 1 << 10  // bijz, naamval
    Bijzonder     Tag = 1 << 10  // bijz, naamval
    Prenominal    Tag = 1 << 11 // prenom, positie
    Prenominaal   Tag = 1 << 11 // prenom, positie
    Nominal       Tag = 1 << 12 // nom, positie
    Nominaal      Tag = 1 << 12 // nom, positie
    WithE         Tag = 1 << 14 // met-e, buiging
    MetE          Tag = 1 << 14 // met-e, buiging
    Postnominal   Tag = 1 << 16 //  post, positie
    Postnominaal  Tag = 1 << 16 //  post, positie
    WithS         Tag = 1 << 17 // met-s, buiging
    MetS          Tag = 1 << 17 // met-s, buiging
    Free          Tag = 1 << 30 // vrij, positie
    Vrij          Tag = 1 << 30 // vrij, positie
    Comparative   Tag = 1 << 31 // comp, graad
    Vergelijkend  Tag = 1 << 31 // comp, graad
  Verb         Tag =   4 * TypePart // WW
  Werkwoord    Tag =   4 * TypePart // WW
    Finite            Tag = 1 << 0 // pv, wvorm
    Persoonsvorm      Tag = 1 << 0 // pv, wvorm
    Infinitive        Tag = 1 << 1 // inf, wvorm
    OnbepaaldeWijs    Tag = 1 << 1 // inf, wvorm
    PresentParticiple Tag = 1 << 2 // od, wvorm
    OnvoltDeelwoord   Tag = 1 << 2 // od, wvorm
    //NoDeclension      Tag = 1 << 3 // zonder, buiging
    Present           Tag =  1 << 4 // tgw, pvtijd
    TegenwoordigeTijd Tag =  1 << 4 // tgw, pvtijd
    Past              Tag =  1 << 5 // verl, pvtijd
    VerledenTijd      Tag =  1 << 5 // verl, pvtijd
    Subjunctive       Tag =  1 << 6 // conj, pvtijd
    Conjunctief       Tag =  1 << 6 // conj, pvtijd
    //Singular          Tag = 1 << 7 // ev, pvagr
    //Plural            Tag = 1 << 8 // mv, pvagr
    WithT             Tag = 1 << 9 // met-t, pvagr
    MetT              Tag = 1 << 9 // met-t, pvagr
    PastParticiple    Tag = 1 << 10  // vd, wvorm
    VoltDeelwoord     Tag = 1 << 10  // vd, wvorm
    //Prenominal        Tag = 1 << 11 // prenom, positie
    //Nominal           Tag = 1 << 12 // nom, positie
    //WithE             Tag = 1 << 14 // met-e, buiging
    WithoutN          Tag = 1 << 15 // zonder-n, getal-n
    ZonderN           Tag = 1 << 15 // zonder-n, getal-n
    WithN             Tag = 1 << 16 // mv-n, getal-n
    MetN              Tag = 1 << 16 // mv-n, getal-n
    PastWithT         Tag = 1 << 17 // verl-met-t, ww
    VerledenMetT      Tag = 1 << 17 // verl-met-t, ww
    //Free       Tag =1<<30 // vrij, positie
  Numeral      Tag =   8 * TypePart // TW
  Telwoord     Tag =   8 * TypePart // TW
    Cardinal      Tag = 1 << 0 // hoofd, numtype
    Hoofdtelwoord Tag = 1 << 0 // hoofd, numtype
    Ordinal       Tag = 1 << 1 // rang, numtype
    Rangtelwoord  Tag = 1 << 1 // rang, numtype
    //Base          Tag = 1 << 4 // basis, graad
    //Diminutive    Tag = 1 << 5 // dim, graad
    //Standard      Tag = 1 << 9 // stan, naamval
    //Special       Tag = 1 << 10  // bijz, naamval
    //Prenominal    Tag = 1 << 11 // prenom, positie
    //Nominal       Tag = 1 << 12 // nom, positie
    //WithoutN      Tag = 1 << 15 // zonder-n, getal-n
    //WithN         Tag = 1 << 16 // mv-n, getal-n
    //Free          Tag = 1 << 30 // vrij, positie
  Pronoun      Tag =  1 << 4 * TypePart // VNW
  Voornaamwoord Tag=  1 << 4 * TypePart // VNW
    Personal            Tag = 1 << 0 // pers, wvtype
    Persoonlijk         Tag = 1 << 0 // pers, wvtype
    //Superlative         Tag = 1 << 1 // sup, graad
    //Agreement           Tag = 1 << 2 // agr, npagr
    //NoDeclension        Tag = 1 << 3 // zonder, buiging
    //Diminutive          Tag = 1 << 5 // dim, graad
    //Neuter              Tag = 1 << 6 // onz, genus
    //Singular            Tag = 1 << 7 // zonder-n
    //Plural              Tag = 1 << 8 // mv-n
    //Standard            Tag = 1 << 9 // stan, naamval
    //Genitive            Tag = 1 << 10  // gen, naamval
    //Prenominal          Tag = 1 << 11 // prenom, positie
    //Nominal             Tag = 1 << 12 // nom, positie
    Numeric             Tag = 1 << 13 // getal, getal
    Getal               Tag = 1 << 13 // getal, getal
    //WithE               Tag = 1 << 14 // met-e, buiging
    //WithoutN            Tag = 1 << 15 // zonder-n, getal-n
    //WithN               Tag = 1 << 16 // mv-n, getal-n
    //WithS               Tag = 1 << 17 // met-s, buiging
    Masculine           Tag = 1 << 18 // masc, genus
    Manlijk             Tag = 1 << 18 // masc, genus
    Feminine            Tag = 1 << 19 // fem, genus
    Vrouwlijk           Tag = 1 << 19 // fem, genus
    QuesOrRel           Tag = 1 << 20  // underspecified vb, wvtype
    VragOfBetr          Tag = 1 << 20  // underspecified vb, wvtype
    Reciprocal          Tag = 1 << 21 // recip, wvtype
    OnderlingWederkerig Tag = 1 << 21 // recip, wvtype
    PersOrRefl          Tag = 1 << 22 // underspecified pr, wvtype
    PersOfWeder         Tag = 1 << 22 // underspecified pr, wvtype
    Exclamative         Tag = 1 << 23 // excl, wvtype
    Uitroepend          Tag = 1 << 23 // excl, wvtypw
    Reflexive           Tag = 1 << 24 // refl, wvtype
    Wederkerig          Tag = 1 << 24 // refl, wvtype
    SingularMascNeut    Tag = 1 << 25 // evmo, npagr
    EnkelvoudigManOnz   Tag = 1 << 25 // evmo, npagr
    SingularFeminine    Tag = 1 << 26 // evf, npagr
    EnkelvoudigVrouwlijk Tag = 1 << 26 // evf, npagr
    Possessive          Tag = 1 << 27 // bez, wvtype
    Bezittelijk         Tag = 1 << 27 // bez, wvtype
    //Dative              Tag = 1 << 28 // dat, naamval
    Relative            Tag = 1 << 29 // betr, wvtype
    Betrekkelijk        Tag = 1 << 29// betr, wvtype
    //Free                Tag = 1 << 30 // vrij, positie
    //Comparative         Tag = 1 << 31 // comp, graad
    Indefinite          Tag = 1 << 32 // onbep, wvtype
    Onbepaald           Tag = 1 << 32 // onbep, wvtype
    ThirdPersonNeuter    Tag = 1 << 33
    DerdePersoonOnzijdig Tag = 1 << 33
    ThirdPersonPlural   Tag = 1 << 34
    DerdePersoonMeervoud Tag = 1 << 34
    FirstPerson         Tag = 1 << 35 //
    EerstePersoon       Tag = 1 << 35 //
    SecondPerson        Tag = 1 << 36
    TweedePersoon       Tag = 1 << 36
    ThirdPerson         Tag = 1 << 37
    DerdePersoon        Tag = 1 << 37
    ThirdPersonMale     Tag = 1 << 38
    DerdePersoonManlk   Tag = 1 << 38
    ThirdPersonFemale   Tag = 1 << 39
    DerdePersoonVrwlk   Tag = 1 << 39
    Nominative          Tag = 1 << 40 // nomin, naamval
    Nominatief          Tag = 1 << 40 // nomin, naamval
    FullForm            Tag = 1 << 41 // vol, status
    Volledig            Tag = 1 << 41 // vol, status
    Oblique             Tag = 1 << 42 // obl, naamval
    ReducedForm         Tag = 1 << 43 // red, status
    Gereduceerd         Tag = 1 << 43 // red, status
    StressedForm        Tag = 1 << 44 // red, status
    Benadrukkend        Tag = 1 << 44 // red, status
    Demonstrative       Tag = 1 << 45 // aanw, wvtype
    Aanwijzend          Tag = 1 << 45 // aanw, wvtype
    Pronomen            Tag = 1 << 46 // pron, pdtype
    AdverbialPronomen   Tag = 1 << 47 // adv-pron, pdtype
    BijwoordelijkeBepaling Tag=1<< 47 // adv-pron, pdtype
    Determiner          Tag = 1 << 48 // det, pdtype
    Bepaling            Tag = 1 << 48 // det, pdtype
    GradableDeterminer  Tag = 1 << 49 // grad, pdtype
    SingularNeuter      Tag = 1 << 50 // evon, npagr
    Interrogative       Tag = 1 << 51 // vrag, wvtype
    Vragend             Tag = 1 << 51 // vrag, wvtype

  Article      Tag =  1 << 5 * TypePart // LID
  Lidwoord     Tag =  1 << 5 * TypePart // LID
    Definite             Tag = 1 << 0 // bep, lwtype
    Bepaald              Tag = 1 << 0 // bep, lwtype
    Agreement            Tag = 1 << 2 // agr, npagr
    Overeenstemmend      Tag = 1 << 2 // agr, npagr
    EnkelvoudigOnzijdig  Tag = 1 << 3 // evon, npagr
    NotSingularNotMasc   Tag = 1 << 5 // rest3, npagr
    NietEnkelNietMan     Tag = 1 << 5 // rest3, npagr
    PluralFeminine       Tag = 1 << 7 // mv, npagr
    MeervoudVrouwlijk    Tag = 1 << 7 // mv, npagr
    //Standard              Tag = 1 << 9 // stan, naamval
    //Standaard             Tag = 1 << 9 // stan, naamval
    //Genitive              Tag = 1 << 10  // gen, naamval
    //Genitief              Tag = 1 << 10  // gen, naamval
    //Dative                Tag = 1 << 28 // dat, naamval
    //Indefinite            Tag = 1 << 32 // onbep, lwtype
    //Onbepaald             Tag = 1 << 12 // onbep, lwtype
    //SingularMascNeut      Tag = 1 << 25 // evmo, npagr
    //SingularFeminine      Tag = 1 << 26 // evf, npagr
    //SingularNeuter        Tag = 1 << 50 // evon, npagr
  Preposition  Tag =  64 * TypePart // VZ
  Voorzetsel   Tag =  64 * TypePart // VZ
    Initial              Tag =    1 // init, vztype
    Voorafgaand          Tag =    1 //  init, vztype
    Melted               Tag =    2 // versm, vztype
    Versmeltend          Tag =    2 // versm, vztype
    Final                Tag =    4 // fin, vztype
    Volgend              Tag =    4 // fin, vztype
  Conjunction  Tag = 128 * TypePart // VG
  Voegwoord    Tag = 128 * TypePart // VG
    Coordinating         Tag =    1 // neven
    Neven                Tag =    1 // neven
    Subordinating        Tag =    2 // onder
    Ondergeschikt        Tag =    2 // onder
  Adverb       Tag = 256 * TypePart // BW
  Bijwoord     Tag = 256 * TypePart // BW

  Interjection Tag = 512 * TypePart // TSW
  Tussenwoord  Tag = 512 * TypePart // TSW

  SPEC         Tag = 1024 * TypePart // SPEC for Names and unknown
    PartialOwn           Tag = 1 // SPEC(deeleigen) for people's names
    Deeleigen            Tag = 1 // SPEC(deeleigen) for people's names
    Symbol               Tag = 2 // SPEC(symb) for numbers, dollar-signs, euro-signs etc.
    Foreign              Tag = 4 // SPEC(vreemd) foreign
    Vreemd               Tag = 4 // SPEC(vreemd) foreign
    Abbreviation         Tag = 8 // SPEC(afk) abbreviation?
    Afkorting            Tag = 8 // SPEC(afk) abbreviation?
)

// Frog returns POStags as type(attribute,attribute,…), all
// possibilities as single-attrbutes are layed out below in a map.
var POSStringToTag = map[string]Tag  {
   "N"             : Noun,
   "N(soort)"      : Noun | Common,
   "N(eigen)"      : Noun | Property,
   "N(basis)"      : Noun | Base,
   "N(zijd)"       : Noun | NonNeuter,
   "N(genus)"      : Noun | Genus,
   "N(dim)"        : Noun | Diminutive,
   "N(onz)"        : Noun | Neuter,
   "N(ev)"         : Noun | Singular,
   "N(mv)"         : Noun | Plural,
   "N(stan)"       : Noun | Standard,
   "N(gen)"        : Noun | Genitive,
   "N(dat)"        : Noun | Dative,

   "ADJ"           : Adjective,
   "ADJ(prenom)"   : Adjective | Prenominal,
   "ADJ(nom)"      : Adjective | Nominal,
   "ADJ(postnom)"  : Adjective | Postnominal,
   "ADJ(vrij)"     : Adjective | Free,
   "ADJ(basis)"    : Adjective | Base,
   "ADJ(comp)"     : Adjective | Comparative,
   "ADJ(sup)"      : Adjective | Superlative,
   "ADJ(dim)"      : Adjective | Diminutive,
   "ADJ(met-e)"    : Adjective | WithE,
   "ADJ(met-s)"    : Adjective | WithS,
   "ADJ(zonder-n)" : Adjective | WithoutN,
   "ADJ(mv-n)"     : Adjective | Plural,
   "ADJ(stan)"     : Adjective | Standard,
   "ADJ(bijz)"     : Adjective | Special,
   "ADJ(zonder)"   : Adjective | NoDeclension,

   "WW"            : Verb,
   "WW(pv)"        : Verb | Finite,
   "WW(inf)"       : Verb | Infinitive,
   "WW(od)"        : Verb | PresentParticiple,
   "WW(vd)"        : Verb | PastParticiple,
   "WW(tgw)"       : Verb | Present,
   "WW(verl)"      : Verb | Past,
   "WW(conj)"      : Verb | Subjunctive,
   "WW(ev)"        : Verb | Singular,
   "WW(mv)"        : Verb | Plural,
   "WW(met-t)"     : Verb | WithT,
   "WW(prenom)"    : Verb | Prenominal,
   "WW(nom)"       : Verb | Nominal,
   "WW(vrij)"      : Verb | Free,
   "WW(zonder)"    : Verb | NoDeclension,
   "WW(met-e)"     : Verb | WithE,
   "WW(zonder-n)"  : Verb | WithoutN,
   "WW(mv-n)"      : Verb | WithN,
   "WW(verl-met-t)": Verb | PastWithT,

   "TW"            : Numeral,
   "TW(hoofd)"     : Numeral | Cardinal,
   "TW(rang)"      : Numeral | Ordinal,
   "TW(prenom)"    : Numeral | Prenominal,
   "TW(nom)"       : Numeral | Nominal,
   "TW(vrij)"      : Numeral | Free,
   "TW(zonder-n)"  : Numeral | WithoutN,
   "TW(mv-n)"      : Numeral | WithN,
   "TW(basis)"     : Numeral | Base,
   "TW(dim)"       : Numeral | Diminutive,
   "TW(stan)"      : Numeral | Standard,
   "TW(bijz)"      : Numeral | Special,

   "VNW"           : Pronoun,
   "VNW(pers)"     : Pronoun | Personal,
   "VNW(refl)"     : Pronoun | Reflexive,
   "VNW(pr)"       : Pronoun | PersOrRefl,
   "VNW(recip)"    : Pronoun | Reciprocal,
   "VNW(bez)"      : Pronoun | Possessive,
   "VNW(vrag)"     : Pronoun | Interrogative,
   "VNW(betr)"     : Pronoun | Relative,
   "VNW(vb)"       : Pronoun | QuesOrRel,
   "VNW(excl)"     : Pronoun | Exclamative,
   "VNW(aanw)"     : Pronoun | Demonstrative,
   "VNW(onbep)"    : Pronoun | Indefinite,
   "VNW(pron)"     : Pronoun | Pronomen,
   "VNW(adv-pron)" : Pronoun | AdverbialPronomen,
   "VNW(det)"      : Pronoun | Determiner,
   "VNW(grad)"     : Pronoun | GradableDeterminer,
   "VNW(stan)"     : Pronoun | Standard,
   "VNW(nomin)"    : Pronoun | Nominative,
   "VNW(obl)"      : Pronoun | Oblique,
   "VNW(gen)"      : Pronoun | Genitive,
   "VNW(dat)"      : Pronoun | Dative,
   "VNW(vol)"      : Pronoun | FullForm,
   "VNW(red)"      : Pronoun | ReducedForm,
   "VNW(nadr)"     : Pronoun | StressedForm,
   "VNW(1)"        : Pronoun | FirstPerson,
   "VNW(2)"        : Pronoun | SecondPerson,
   "VNW(3)"        : Pronoun | ThirdPerson,
   "VNW(3m)"       : Pronoun | ThirdPersonMale,
   "VNW(3v)"       : Pronoun | ThirdPersonFemale,
   "VNW(3o)"       : Pronoun | ThirdPersonNeuter,
   "VNW(3p)"       : Pronoun | ThirdPersonPlural,
   "VNW(ev)"       : Pronoun | Singular,
   "VNW(mv)"       : Pronoun | Plural,
   "VNW(getal)"    : Pronoun | Numeric,
   "VNW(masc)"     : Pronoun | Masculine,
   "VNW(fem)"      : Pronoun | Feminine,
   "VNW(onz)"      : Pronoun | Neuter,
   "VNW(prenom)"   : Pronoun | Prenominal,
   "VNW(post)"     : Pronoun | Postnominal,
   "VNW(nom)"      : Pronoun | Nominal,
   "VNW(free)"     : Pronoun | Free,
   "VNW(zonder)"   : Pronoun | NoDeclension,
   "VNW(met-e)"    : Pronoun | WithE,
   "VNW(met-s)"    : Pronoun | WithS,
   "VNW(zonder-n)" : Pronoun | WithoutN,
   "VNW(mv-n)"     : Pronoun | WithN,
   "VNW(basis)"    : Pronoun | Base,
   "VNW(comp)"     : Pronoun | Comparative,
   "VNW(sup)"      : Pronoun | Superlative,
   "VNW(dim)"      : Pronoun | Diminutive,
   "VNW(agr)"      : Pronoun | Agreement,
   "VNW(evmo)"     : Pronoun | SingularMascNeut,
   "VNW(evf)"      : Pronoun | SingularFeminine,
   "VNW(evon)"     : Pronoun | SingularNeuter,
   "VNW(vrij)"     : Pronoun | SingularNeuter,

   "LID"           : Article,
   "LID(bep)"      : Article | Definite,
   "LID(onbep)"    : Article | Indefinite,
   "LID(stan)"     : Article | Standard,
   "LID(gen)"      : Article | Genitive,
   "LID(dat)"      : Article | Dative,
   "LID(agr)"      : Article | Agreement,
   "LID(evon)"     : Article | SingularNeuter,
   "LID(evmo)"     : Article | SingularMascNeut,
   "LID(evf)"      : Article | SingularFeminine,
   "LID(mv)"       : Article | PluralFeminine,

   "VZ"            : Preposition,
   "VZ(init)"      : Preposition | Initial,
   "VZ(versm)"     : Preposition | Melted,
   "VZ(fin)"       : Preposition | Final,

   "VG"            : Conjunction,
   "VG(neven)"     : Conjunction | Coordinating,
   "VG(onder)"     : Conjunction | Subordinating,

   "BW"            : Adverb,
   "BW()"          : Adverb,

   "TSW"           : Interjection,
   "TSW()"         : Interjection,

   "SPEC"          : SPEC,
   "SPEC(deeleigen)": SPEC | PartialOwn,
   "SPEC(symb)"    : SPEC | Symbol,
   "SPEC(vreemd)"  : SPEC | Foreign,
   "SPEC(afk)"     : SPEC | Abbreviation,

}

// The reverse of the above map is available to! One can use:
// TagToPOSString[ Article | Genitive ] to get "LID(gen)".
var TagToPOSString map[Tag]string = reversePOSMap( POSStringToTag )
func reversePOSMap ( mp map[string]Tag ) map[Tag]string {
  rmp := make( map[Tag]string, len( mp ) )
  for key, val := range mp {
    rmp[ val ] = key
  }
  return rmp
}

func POSEncode( tag string ) Tag {
 // tag has form: type(subtype1,subtype2,…)
 // the idea is to OR-together POSstringToTag( type(subtype<n>))
 // and return that
 // so get the type using regexp,
 end := strings.Index( tag, "(" )
 if end == -1 {
   log.Printf( "(frog) Error interpreting POSTag %s has a missing (\n", tag )
   return 0
 }
 POSType := tag[ 0:end ]
 subtypeend := strings.Index( tag[ end: ], ")" )
 if subtypeend == -1 {
   log.Printf( "(frog) Error interpreting POSTag %s has a missing )\n", tag )
   return 0
 }
 Subtypes := strings.Split( tag[ end+1:(end+subtypeend) ], "," )
 var POSEncoded Tag
 for _, stype := range Subtypes {
    if len( stype ) == 2 && stype[:1] == "2" {
      stype = "2"
    }
    POSEncoded |= POSStringToTag[ POSType + "(" + stype + ")" ]
 }
 return POSEncoded
}
func POSDecode( tag Tag ) string {
   // decoding is checking every bit in tag in the lower part, keeping the higher part in
   POSTypeTag := tag & TypePartMask
   decoded := TagToPOSString[ POSTypeTag ]
   decoded = strings.Replace( decoded, "()", "", 1 )
   if decoded == "" {
     log.Printf( "(frog)Can't determine POSType from %b\n", POSTypeTag )
   }
   decoded = decoded + "("
   POSType := decoded
   numelts := 0
   for bitno := 0; bitno < TypeShift; bitno += 1 {
     mask := (1 << bitno | TypePartMask) & tag
     if mask & ^TypePartMask > 0 {
       if numelts > 0 {
         decoded += ","
       }
       stype := strings.Replace( strings.Replace( TagToPOSString[ mask ], POSType, "", 1 ), ")", "", 1 )
       if len( stype ) == 2 && stype[:1] == "2" {
         stype = "2"
       }
       decoded += stype
       numelts += 1
     }
   }
   decoded += ")"
   return decoded
}
func (tok *Token) IsIt( properties Tag ) bool {
  return tok.POSEncoded & properties == properties
}
func (tok *Token) IsVerb() bool {
   return tok.IsIt( Verb )
}
func (tok *Token) IsFinite() bool {
   return tok.IsIt( Verb | Finite )
}

func (tok *Token) IsAdj() bool {
   return tok.IsIt( Adjective )
}

func (tok *Token) IsNoun() bool {
   return tok.IsIt( Noun )
}

func (tok *Token) IsPronoun() bool {
   return tok.IsIt( Pronoun )
}

func (tok *Token) IsNumber() bool {
   return tok.POSEncoded & Numeral == Numeral ||
          tok.POSEncoded & (SPEC | Symbol) == SPEC | Symbol
}

func (tok *Token) IsAdverb() bool {
   return tok.POSEncoded & Adverb == Adverb
}

func (tok *Token) IsInterjection() bool {
   return tok.POSEncoded & Interjection == Interjection
}

// some greeting words are a name!
func (tok *Token) IsGreeting( sen Sentence ) bool {
   vnwposition := -1
   switch strings.ToLower( tok.Token ) {
       case "hai", "haie", "hoi", "hi", "ok", "hallo", "dus":
         for _, word := range sen {
            if tok.IsPronoun() && vnwposition == -1 {
               vnwposition = word.Position
               break
            }
         }
   }
   return tok.Position == vnwposition - 1
}

func (sen *Sentence) IsDenying() bool {
    var val bool
    for _, word := range *sen {
        val = val ||
              ((word.IsAdverb() || word.IsInterjection()) &&
               (word.Token == "nee" || word.Token == "niet" || word.Token == "nooit"))
    }
    return val
}

func (sen *Sentence) IsQuestion() (bool, error) {
    pvposition := -1  // WW(pv,…
    vnwposition := -1 // PNW(…, the first
    for _, word := range *sen {
      if word.IsFinite() && pvposition == -1 {
         pvposition = word.Position
      }
      if word.IsPronoun() && vnwposition == -1 {
         vnwposition = word.Position
      }
    }
    if pvposition == -1 || vnwposition == -1 {
      return false, errors.New( "Unable to determine a result; not a complete sentence" )
    } else {
      return pvposition < vnwposition, nil
    }
}

// even in case of an error, Analyse returns an array of sentences
func (rcvr *FrogClient) Analyse( text string ) ( sents []Sentence, err error ) {
    sentence := make( Sentence )
    if rcvr == nil {
      sents = append( sents, sentence )
      return sents, nil
    }
    conn := rcvr.Connection
    fmt.Fprintf( conn, text + "\n" )
    fmt.Fprintf( conn, "EOT" + "\n" )

    reader := bufio.NewReader(conn)
    for {
      var line string
      if line, err = reader.ReadString( '\n' ); err != nil {
         if len( sents ) == 0 {
            sents = append( sents, sentence )
         }
         return sents, err
      }
      //fmt.Printf( "(frog) Read from remote: %s", line )
      if len( line ) == 1 {
         sents = append( sents, sentence )
         sentence = make( Sentence )
      } else if line == "READY\n" {
         break
      } else {
         token, err := parseInput( line )
         if err != nil {
             return sents, err
         }
         if _, present := sentence[ token.Token ]; present {
           verss := []string{"-1","-2","-3","-4","-5","-6","-7","-8","-9"}
           for _, vers := range verss {
              if _, present := sentence[ token.Token + vers ]; !present {
                 sentence [ token.Token + vers ] = token // avoid collisions
                 break
              }
           }
         } else {
           sentence [ token.Token ] = token
         }
      }
    }
    return sents, nil
}

func parseInput( line string ) ( Token, error ) {
    var t Token
    var err error

    size := len( line )
    line = line[ :size - 1 ]
    columns := strings.Split( line, "\t" )

    if t.Position, err = strconv.Atoi( columns[ 0 ] ); err != nil {
            return t, err
    }

    if t.POSConfidence, err = strconv.ParseFloat( columns[ 5 ], 64 ); err != nil {
            return t, err
    }

    t.Token           = columns[ 1 ]
    t.Lemma           = columns[ 2 ]
    t.POSTag          = columns[ 4 ]
    t.NamedEntityType = columns[ 6 ]
    t.POSEncoded      = POSEncode( t.POSTag )
    return t, nil
}
