all: dirs frogger

frogger: src/frog/frog.go src/frogger/frogger.go src/frog/frog_test.go
	GOPATH=`pwd` go get $@
	GOPATH=`pwd` go build $@
frogger.arm: src/frogger/frogger.go
	GOOS=linux GOARCH=arm GOPATH=`pwd` go build -o $@ frogger
dirs: src/frog src/frogger

src/frog/frog_test.go:
	-[ ! -r "$@" ] && ln -s ../../frog_test.go src/frog/
src/frog/frog.go:
	-[ ! -r "$@" ] && ln -s ../../frog.go src/frog/

src/frog:
	-[ ! -d "$@" ] && mkdir -p "$@"
