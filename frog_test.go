package frog

    import (
      "os"
      "log"
      "testing"
    )

    func TestMain(m *testing.M) {
       os.Exit(m.Run())
    }

    func TestFrog( t *testing.T ) {
       fl, err := NewFrogClient( os.Getenv( "FROG_CONNECT" ) )
       if err != nil {
          t.Error( "Error connecting to Frog Server", err )
       }
       sents, err := fl.Analyse( "Hi, ik ben patty" )
       if err != nil {
          t.Error( "Analysing failed", err )
       }
       res := sents[ 0 ]
       if res[ "ben" ].Token != "ben" {
          t.Error( "Finding token for ben failed", err )
       }
       tok := res[ "ben" ]
       if !tok.IsVerb( ) {
          t.Error( "Testing ben for being a verb failed" )
       }

       sents, err = fl.Analyse( "ik ben vierentwintig jaar" )
       if err != nil {
          t.Error( "Analysing failed", err )
       }
       res = sents[ 0 ]
       if res[ "vierentwintig" ].Token != "vierentwintig" {
          t.Error( "Finding token for vierentwintig failed", err )
       }
       tok = res[ "vierentwintig" ]
       if !tok.IsNumber( ) {
          t.Error( "Testing vierentwintig for being a number failed" )
       }

       sents, err = fl.Analyse( "ik ben niet wakker" )
       if err != nil {
          t.Error( "Analysing failed", err )
       }
       res = sents[ 0 ]
       if !res.IsDenying() {
          t.Error( "Testing for denial in sentence failed" )
       }
       isaq, err := res.IsQuestion(); if err != nil {
          t.Error( "Testing for question a complete sentence 1 failed: ", err )
       }
       if isaq {
          t.Error( "Testing for question in non-questioning sentence failed" )
       }

       sents, err = fl.Analyse( "ben je wel wakker" )
       if err != nil {
          t.Error( "Analysing failed", err )
       }
       res = sents[ 0 ]
       isaq, err = res.IsQuestion(); if err != nil {
          t.Error( "Testing for question a complete sentence failed: ", err )
       }
       if !isaq {
          t.Error( "Testing for question in questioning sentence failed" )
       }

       sents, err = fl.Analyse( "30% en meer" )
       if err != nil {
          t.Error( "Analysing failed", err )
       }
       if len( sents ) != 2 {
          t.Error( "Length of analysis is off, must be 2", len( sents ) )
       }

       if POSStringToTag[ "VNW(mv)" ] - Pronoun != Plural {
          t.Error( "POSStringToTag is off", POSStringToTag[ "VNW(mv)" ], Plural )
       }
       if POSStringToTag[ "VNW(mv)" ] & Plural != Plural {
          t.Error( "POSStringToTag is off", POSStringToTag[ "VNW(mv)" ], Plural )
       }
       if TagToPOSString[  Pronoun | Plural ] != "VNW(mv)" {
          t.Error( "TagToPOSString is off", TagToPOSString[ Pronoun | Plural ], "VNW(mv)" )
       }
       testing := []string{
"N(soort,ev,basis,zijd,stan)",
"N(soort,ev,basis,onz,stan)",
"N(soort,ev,dim,onz,stan)",
"N(soort,ev,basis,gen)",
"N(soort,ev,dim,gen)",
"N(soort,ev,basis,dat)",
"N(soort,mv,basis)",
"N(soort,mv,dim)",
"N(eigen,ev,basis,zijd,stan)",
"N(eigen,ev,basis,onz,stan)",
"N(eigen,ev,dim,onz,stan)",
"N(eigen,ev,basis,gen)",
"N(eigen,ev,dim,gen)",
"N(eigen,ev,basis,dat)",
"N(eigen,mv,basis)",
"N(eigen,mv,dim)",
"N(soort,ev,basis,genus,stan)",
"N(eigen,ev,basis,genus,stan)",
"ADJ(prenom,basis,zonder)",
"ADJ(prenom,basis,met-e,stan)",
"ADJ(prenom,basis,met-e,bijz)",
"ADJ(prenom,comp,zonder)",
"ADJ(prenom,comp,met-e,stan)",
"ADJ(prenom,comp,met-e,bijz)",
"ADJ(prenom,sup,zonder)",
"ADJ(prenom,sup,met-e,stan)",
"ADJ(prenom,sup,met-e,bijz)",
"ADJ(nom,basis,zonder,zonder-n)",
"ADJ(nom,basis,zonder,mv-n)",
"ADJ(nom,basis,met-e,zonder-n,bijz)",
"ADJ(nom,basis,met-e,mv-n)",
"ADJ(nom,comp,zonder,zonder-n)",
"ADJ(nom,comp,met-e,zonder-n,stan)",
"ADJ(nom,comp,met-e,zonder-n,bijz)",
"ADJ(nom,comp,met-e,mv-n)",
"ADJ(nom,sup,zonder,zonder-n)",
"ADJ(nom,sup,met-e,zonder-n,stan)",
"ADJ(nom,sup,met-e,zonder-n,bijz)",
"ADJ(nom,sup,met-e,mv-n)",
"ADJ(postnom,basis,zonder)",
"ADJ(postnom,basis,met-s)",
"ADJ(postnom,comp,zonder)",
"ADJ(postnom,comp,met-s)",
"ADJ(vrij,basis,zonder)",
"ADJ(vrij,comp,zonder)",
"ADJ(vrij,sup,zonder)",
"ADJ(vrij,dim,zonder)",
"WW(pv,tgw,ev)",
"WW(pv,tgw,mv)",
"WW(pv,tgw,met-t)",
"WW(pv,verl,ev)",
"WW(pv,verl,mv)",
"WW(pv,verl-met-t)",
"WW(pv,conj,ev)",
"WW(inf,prenom,zonder)",
"WW(inf,prenom,met-e)",
"WW(inf,nom,zonder,zonder-n)",
"WW(inf,vrij,zonder)",
"WW(vd,prenom,zonder)",
"WW(vd,nom,met-e,zonder-n)",
"WW(vd,nom,met-e,mv-n)",
"WW(vd,vrij,zonder)",
"WW(od,prenom,zonder)",
"WW(od,prenom,met-e)",
"WW(od,nom,met-e,zonder-n)",
"WW(od,nom,met-e,mv-n)",
"WW(od,vrij,zonder)",
"VNW(pers,pron,nomin,vol,1,ev)",
"VNW(pers,pron,nomin,nadr,1,ev)",
"VNW(pers,pron,nomin,red,1,ev)",
"VNW(pers,pron,nomin,vol,1,mv)",
"VNW(pers,pron,nomin,nadr,1,mv)",
"VNW(pers,pron,nomin,red,1,mv)",
"VNW(pers,pron,nomin,vol,2,getal)",
"VNW(pers,pron,nomin,nadr,2,getal)",
"VNW(pers,pron,nomin,red,2,getal)",
"VNW(pers,pron,nomin,vol,3,ev,masc)",
"VNW(pers,pron,nomin,nadr,3m,ev,masc)",
"VNW(pers,pron,nomin,red,3,ev,masc)",
"VNW(pers,pron,nomin,red,3p,ev,masc)",
"VNW(pers,pron,nomin,vol,3v,ev,fem)",
"VNW(pers,pron,nomin,nadr,3v,ev,fem)",
"VNW(pers,pron,nomin,vol,3p,mv)",
"VNW(pers,pron,nomin,nadr,3p,mv)",
"VNW(pers,pron,obl,vol,3,ev,masc)",
"VNW(pers,pron,obl,nadr,3m,ev,masc)",
"VNW(pers,pron,obl,red,3,ev,masc)",
"VNW(pers,pron,obl,vol,3,getal,fem)",
"VNW(pers,pron,obl,nadr,3v,getal,fem)",
"VNW(pers,pron,obl,red,3v,getal,fem)",
"VNW(pers,pron,obl,vol,3p,mv)",
"VNW(pers,pron,obl,nadr,3p,mv)",
"VNW(pers,pron,stan,red,3,ev,onz)",
"VNW(pers,pron,stan,red,3,ev,fem)",
"VNW(pers,pron,stan,red,3,mv)",
"VNW(pers,pron,gen,vol,1,ev)",
"VNW(pers,pron,gen,vol,1,mv)",
"VNW(pers,pron,gen,vol,2,getal)",
"VNW(pers,pron,gen,vol,3m,ev)",
"VNW(pers,pron,gen,vol,3v,getal)",
"VNW(pers,pron,gen,vol,3p,mv)",
"VNW(pr,pron,obl,vol,1,ev)",
"VNW(pr,pron,obl,nadr,1,ev)",
"VNW(pr,pron,obl,red,1,ev)",
"VNW(pr,pron,obl,vol,1,mv)",
"VNW(pr,pron,obl,nadr,1,mv)",
"VNW(pr,pron,obl,vol,2,getal)",
"VNW(pr,pron,obl,nadr,2,getal)",
"VNW(refl,pron,obl,red,3,getal)",
"VNW(refl,pron,obl,nadr,3,getal)",
"VNW(recip,pron,obl,vol,mv)",
"VNW(recip,pron,gen,vol,mv)",
"VNW(bez,det,stan,vol,1,ev,prenom,zonder,agr)",
"VNW(bez,det,stan,red,1,ev,prenom,zonder,agr)",
"VNW(bez,det,stan,vol,1,mv,prenom,zonder,evon)",
"VNW(bez,det,stan,vol,2,getal,prenom,zonder,agr)",
"VNW(bez,det,stan,vol,3,ev,prenom,zonder,agr)",
"VNW(bez,det,stan,red,3,ev,prenom,zonder,agr)",
"VNW(bez,det,stan,vol,3,mv,prenom,zonder,agr)",
"VNW(bez,det,stan,red,3,getal,prenom,zonder,agr)",
"VNW(bez,det,gen,vol,1,ev,prenom,zonder,evmo)",
"VNW(bez,det,gen,vol,1,mv,prenom,met-e,evmo)",
"VNW(bez,det,gen,vol,2,getal,prenom,zonder,evmo)",
"VNW(bez,det,gen,vol,3,ev,prenom,zonder,evmo)",
"VNW(bez,det,gen,vol,3v,ev,prenom,zonder,evmo)",
"VNW(bez,det,gen,vol,3p,mv,prenom,zonder,evmo)",
"VNW(bez,det,dat,vol,1,ev,prenom,met-e,evmo)",
"VNW(bez,det,dat,vol,1,ev,prenom,met-e,evf)",
"VNW(bez,det,dat,vol,1,mv,prenom,met-e,evmo)",
"VNW(bez,det,dat,vol,1,mv,prenom,met-e,evf)",
"VNW(bez,det,dat,vol,2,getal,prenom,met-e,evmo)",
"VNW(bez,det,dat,vol,2,getal,prenom,met-e,evf)",
"VNW(bez,det,dat,vol,3,ev,prenom,met-e,evmo)",
"VNW(bez,det,dat,vol,3,ev,prenom,met-e,evf)",
"VNW(bez,det,dat,vol,3v,ev,prenom,met-e,evmo)",
"VNW(bez,det,dat,vol,3v,ev,prenom,met-e,evf)",
"VNW(bez,det,dat,vol,3p,mv,prenom,met-e,evmo)",
"VNW(bez,det,dat,vol,3p,mv,prenom,met-e,evf)",
"VNW(bez,det,stan,vol,1,ev,nom,met-e,zonder-n)",
"VNW(bez,det,stan,vol,1,mv,nom,met-e,zonder-n)",
"VNW(bez,det,stan,vol,2,getal,nom,met-e,zonder-n)",
"VNW(bez,det,stan,vol,3m,ev,nom,met-e,zonder-n)",
"VNW(bez,det,stan,vol,3v,ev,nom,met-e,zonder-n)",
"VNW(bez,det,stan,vol,3p,mv,nom,met-e,zonder-n)",
"VNW(bez,det,stan,vol,1,ev,nom,met-e,mv-n)",
"VNW(bez,det,stan,vol,1,mv,nom,met-e,mv-n)",
"VNW(bez,det,stan,vol,2,getal,nom,met-e,mv-n)",
"VNW(bez,det,stan,vol,3m,ev,nom,met-e,mv-n)",
"VNW(bez,det,stan,vol,3v,ev,nom,met-e,mv-n)",
"VNW(bez,det,stan,vol,3p,mv,nom,met-e,mv-n)",
"VNW(bez,det,dat,vol,1,ev,nom,met-e,zonder-n)",
"VNW(bez,det,dat,vol,1,mv,nom,met-e,zonder-n)",
"VNW(bez,det,dat,vol,2,getal,nom,met-e,zonder-n)",
"VNW(bez,det,dat,vol,ev,nom,met-e,zonder-n)",
"VNW(bez,det,dat,vol,3v,ev,nom,met-e,zonder-n)",
"VNW(bez,det,dat,vol,3p,mv,nom,met-e,zonder-n)",
"VNW(vrag,pron,stan,nadr,3o,ev)",
"VNW(betr,pron,stan,vol,getal)",
"VNW(betr,pron,stan,vol,3,ev)",
"VNW(betr,pron,stan,vol,3o,ev)",
"VNW(betr,pron,gen,vol,3o,ev)",
"VNW(betr,pron,gen,vol,3o,getal)",
"VNW(vb,pron,stan,vol,3p,getal)",
"VNW(vb,pron,stan,vol,3o,ev)",
"VNW(vb,pron,gen,vol,3m,ev)",
"VNW(vb,pron,gen,vol,3v,ev)",
"VNW(vb,pron,gen,vol,3p,mv)",
"VNW(vb,adv-pron,obl,vol,3o,getal)",
"VNW(vb,det,stan,prenom,zonder,evon)",
"VNW(vb,det,stan,nom,met-e,zonder-n)",
"VNW(excl,pron,stan,vol,3,getal)",
"VNW(excl,det,stan,vrij,zonder)",
"VNW(aanw,pron,stan,vol,3o,ev)",
"VNW(aanw,pron,stan,nadr,3o,ev)",
"VNW(aanw,pron,stan,vol,3,getal)",
"VNW(aanw,pron,gen,vol,3m,ev)",
"VNW(aanw,pron,gen,vol,3o,ev)",
"VNW(aanw,adv-pron,obl,vol,3o,getal)",
"VNW(aanw,adv-pron,stan,red,3,getal)",
"VNW(aanw,det,stan,prenom,zonder,evon)",
"VNW(aanw,det,stan,prenom,zonder,agr)",
"VNW(aanw,det,dat,prenom,met-e,evmo)",
"VNW(aanw,det,dat,prenom,met-e,evf)",
"VNW(aanw,det,stan,nom,met-e,zonder-n)",
"VNW(aanw,det,gen,nom,met-e,zonder-n)",
"VNW(aanw,det,dat,nom,met-e,zonder-n)",
"VNW(aanw,det,stan,vrij,zonder)",
"VNW(onbep,pron,stan,vol,3p,ev)",
"VNW(onbep,pron,stan,vol,3o,ev)",
"VNW(onbep,pron,gen,vol,3p,ev)",
"VNW(onbep,adv-pron,obl,vol,3o,getal)",
"VNW(onbep,adv-pron,gen,red,3,getal)",
"VNW(onbep,det,stan,prenom,zonder,evon)",
"VNW(onbep,det,stan,prenom,zonder,agr)",
"VNW(onbep,det,stan,prenom,met-e,ev)",
"VNW(onbep,det,stan,prenom,met-e,mv)",
"VNW(onbep,det,stan,prenom,met-e,agr)",
"VNW(onbep,det,gen,prenom,met-e,mv)",
"VNW(onbep,det,dat,prenom,met-e,evmo)",
"VNW(onbep,det,dat,prenom,met-e,evf)",
"VNW(onbep,grad,stan,prenom,zonder,agr,basis)",
"VNW(onbep,grad,stan,prenom,met-e,agr,basis)",
"VNW(onbep,grad,stan,prenom,met-e,mv,basis)",
"VNW(onbep,grad,stan,prenom,zonder,agr,comp)",
"VNW(onbep,grad,stan,prenom,met-e,agr,sup)",
"VNW(onbep,det,stan,nom,met-e,mv-n)",
"VNW(onbep,det,stan,nom,met-e,zonder-n)",
"VNW(onbep,det,stan,nom,zonder,zonder-n)",
"VNW(onbep,det,gen,nom,met-e,mv-n)",
"VNW(onbep,grad,stan,nom,met-e,mv-n,basis)",
"VNW(onbep,grad,stan,nom,met-e,zonder-n,sup)",
"VNW(onbep,grad,stan,nom,met-e,mv-n,sup)",
"VNW(onbep,grad,stan,nom,zonder,mv-n,dim)",
"VNW(onbep,grad,gen,nom,met-e,mv-n,basis)",
"VNW(onbep,det,stan,vrij,zonder)",
"VNW(onbep,grad,stan,vrij,zonder,basis)",
"VNW(onbep,grad,stan,vrij,zonder,comp)",
"VNW(onbep,grad,stan,vrij,zonder,sup)",
"LID(bep,stan,evon)",
"LID(bep,gen,evmo)",
"LID(bep,dat,evmo)",
"LID(bep,dat,evf)",
"LID(bep,dat,mv)",
"LID(onbep,stan,agr)",
"LID(onbep,gen,evf)",
"VZ(init)",
"VZ(fin)",
"VZ(versm)",
"VG(neven)",
"VG(onder)",
"BW()",
"TSW()",
"TW(hoofd,prenom,stan)",
"TW(hoofd,prenom,bijz)",
"TW(hoofd,nom,zonder-n,basis)",
"TW(hoofd,nom,mv-n,basis)",
"TW(hoofd,nom,zonder-n,dim)",
"TW(hoofd,nom,mv-n,dim)",
"TW(hoofd,vrij)",
"TW(rang,prenom,stan)",
"TW(rang,prenom,bijz)",
"TW(rang,nom,zonder-n)",
"TW(rang,nom,mv-n)",
}
       for _, val := range testing {
           encodedtag := POSEncode( val )
           decodedtag := POSDecode( encodedtag )
           if decodedtag != val && len( decodedtag ) != len( val ) {
   log.Printf( "Tag is              %b for POS %s\n", encodedtag, val )
              t.Error( "Encoding/Decoding didn't work: ", decodedtag, "!=", val )
           }
       }
    }
